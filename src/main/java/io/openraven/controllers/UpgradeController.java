package io.openraven.controllers;

import io.openraven.services.UpgradeService;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.NoSuchJobExecutionException;
import org.springframework.batch.core.launch.NoSuchJobInstanceException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/upgrades/run")
public class UpgradeController {

    private final UpgradeService upgradeService;

    public UpgradeController(UpgradeService upgradeService) {
        this.upgradeService = upgradeService;
    }

    @PostMapping
    public String upgrade() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        return this.upgradeService.start().toString();
    }

    @GetMapping
    public List<Long> list() {
        return this.upgradeService.list();
    }

    @GetMapping("/{id}")
    public List<Long> status(@PathVariable("id") long id) throws NoSuchJobInstanceException {
        return this.upgradeService.status(id);
    }

    @GetMapping("/execution/{jobExecutionId}")
    public Map<Long, String> executionStatus(@PathVariable("jobExecutionId") long jobExecutionId) throws NoSuchJobExecutionException {
        return this.upgradeService.executionStatus(jobExecutionId);
    }
}
