package io.openraven.controllers;

import io.sentry.Sentry;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Map;

@ControllerAdvice
public class ControllerExceptionHandler {

    private static final Map<String, String> STATIC_ERROR_RESPONSE = Map.of("error", "An error occurred");

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Map<String, String>> handleException(Exception e) {
        Sentry.captureException(e);
        return ResponseEntity.internalServerError().body(STATIC_ERROR_RESPONSE);
    }
}
