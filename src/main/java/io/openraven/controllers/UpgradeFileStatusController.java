package io.openraven.controllers;

import io.openraven.models.UpgradeFileStatus;
import io.openraven.services.UpgradeFileStatusService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/upgrades/status")
public class UpgradeFileStatusController {

    private final UpgradeFileStatusService upgradeFileStatusService;

    public UpgradeFileStatusController(UpgradeFileStatusService upgradeFileStatusService) {
        this.upgradeFileStatusService = upgradeFileStatusService;
    }

    @PostMapping("update")
    public UpgradeFileStatus update() {
        return this.upgradeFileStatusService.update();
    }

    @GetMapping("/")
    public UpgradeFileStatus status() {
        return this.upgradeFileStatusService.model();
    }

    @PostMapping("reset")
    public UpgradeFileStatus reset() throws Exception {
        return this.upgradeFileStatusService.reset();
    }
}
