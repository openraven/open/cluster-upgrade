package io.openraven.services;

import io.openraven.jobs.UpgradeJobCreator;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.launch.NoSuchJobExecutionException;
import org.springframework.batch.core.launch.NoSuchJobInstanceException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class UpgradeServiceImpl implements UpgradeService {

    private final UpgradeJobCreator upgradeJobCreator;
    private final JobLauncher jobLauncher;
    private final JobOperator jobOperator;

    public UpgradeServiceImpl(
            UpgradeJobCreator upgradeJobCreator,
            JobLauncher jobLauncher,
            JobOperator jobOperator
    ) {
        this.upgradeJobCreator = upgradeJobCreator;
        this.jobLauncher = jobLauncher;
        this.jobOperator = jobOperator;
    }

    @Override
    public JobExecution start() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        JobParameters jobParameters = new JobParametersBuilder().addDate("date", new Date()).toJobParameters();
        return this.jobLauncher.run(this.upgradeJobCreator.create(), jobParameters);
    }

    @Override
    public List<Long> status(long instanceId) throws NoSuchJobInstanceException {
        return this.jobOperator.getExecutions(instanceId);
    }

    @Override
    public List<Long> list() {
        try {
            return this.jobOperator.getJobInstances(UpgradeJobCreator.UPGRADE_JOB_NAME, 0, Integer.MAX_VALUE);
        } catch (NoSuchJobException noSuchJobException) {
            return Collections.emptyList();
        }
    }

    @Override
    public Map<Long, String> executionStatus(long jobExecutionId) throws NoSuchJobExecutionException {
        return this.jobOperator.getStepExecutionSummaries(jobExecutionId);
    }
}
