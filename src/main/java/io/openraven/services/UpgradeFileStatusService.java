package io.openraven.services;

import io.openraven.models.UpgradeFileStatus;
import org.springframework.web.client.HttpClientErrorException;

public interface UpgradeFileStatusService {
    /** This throws in the 304 case as part of its formal contract. */
    UpgradeFileStatus update() throws HttpClientErrorException;

    UpgradeFileStatus model();

    UpgradeFileStatus reset() throws Exception;

    void writeToZookeeper() throws Exception;
}
