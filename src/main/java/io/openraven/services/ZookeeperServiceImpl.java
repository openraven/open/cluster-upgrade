package io.openraven.services;

import org.apache.curator.framework.CuratorFramework;
import org.springframework.cloud.zookeeper.config.ZookeeperConfigProperties;
import org.springframework.stereotype.Component;

@Component
public class ZookeeperServiceImpl implements ZookeeperService {

    private final CuratorFramework zookeeperClient;
    private final ZookeeperConfigProperties zookeeperConfigProperties;

    public ZookeeperServiceImpl(
            CuratorFramework zookeeperClient,
            ZookeeperConfigProperties zookeeperConfigProperties) {
        this.zookeeperClient = zookeeperClient;
        this.zookeeperConfigProperties = zookeeperConfigProperties;
    }

    @Override
    public CuratorFramework getZookeeperClient() {
        return zookeeperClient;
    }

    @Override
    public String combineRootAndContext() {
        String defaultContext = zookeeperConfigProperties.getDefaultContext();
        String root = zookeeperConfigProperties.getRoot();
        return combine("", root, defaultContext);
    }
}
