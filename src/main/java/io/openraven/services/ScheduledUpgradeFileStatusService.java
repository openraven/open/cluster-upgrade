package io.openraven.services;

import io.openraven.UpgradeConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * This scheduled task will update allow lists from s3.
 * The fixedDelayString is the config value from application.yml or
 * zookeeper. While zookeeper will automatically update this value using
 * RefreshScope properties annotations, this task is not rescheduled. That
 * is a future enhancement. For now, please bounce the rest service after updating
 * zookeeper.
 */
@Component
public class ScheduledUpgradeFileStatusService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduledUpgradeFileStatusService.class);

    private final UpgradeFileStatusService upgradeFileStatusService;
    private final UpgradeService upgradeService;
    private final UpgradeConfig upgradeConfig;

    @Autowired
    public ScheduledUpgradeFileStatusService(
            UpgradeFileStatusService upgradeFileStatusService,
            UpgradeService upgradeService,
            UpgradeConfig upgradeConfig
    ) {
        this.upgradeFileStatusService = upgradeFileStatusService;
        this.upgradeService = upgradeService;
        this.upgradeConfig = upgradeConfig;
    }

    /**
     * @throws Exception - upgradeService can throw any number of job related and io exceptions
     */
    @Scheduled(fixedDelayString = "${openraven.upgrades.fixedDelay}")
    public void update() throws Exception {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<Optional<JobExecution>> submit = executorService.submit(new UpdateCallable(this.upgradeFileStatusService, this.upgradeService));
        Optional<JobExecution> jobExecutionOptional;
        try {
            jobExecutionOptional = submit.get(this.upgradeConfig.getTimeout().getSeconds(), TimeUnit.SECONDS);
        } catch (HttpClientErrorException e) {
            if (!HttpStatus.NOT_MODIFIED.equals(e.getStatusCode())) {
                LOGGER.warn("Exception thrown during upgrade", e);
            }
            return;
        }

        if (jobExecutionOptional.isPresent()) {
            JobExecution jobExecution = jobExecutionOptional.get();
            if (jobExecution.getExitStatus().equals(ExitStatus.COMPLETED)) {
                this.upgradeFileStatusService.writeToZookeeper();
                LOGGER.debug("Upgrade complete");
            } else {
                this.upgradeFileStatusService.model().etag = "";
                LOGGER.debug("Upgrade Failed");
            }
        } else {
            LOGGER.debug("Upgrade didn't run due to no change in helmfile");
        }

    }

    static class UpdateCallable implements Callable<Optional<JobExecution>> {

        private final UpgradeFileStatusService upgradeFileStatusService;
        private final UpgradeService upgradeService;

        UpdateCallable(UpgradeFileStatusService upgradeFileStatusService, UpgradeService upgradeService) {
            this.upgradeFileStatusService = upgradeFileStatusService;
            this.upgradeService = upgradeService;
        }

        @Override
        public Optional<JobExecution> call() throws Exception {
            JobExecution execution;
            LOGGER.debug("Scheduled update of upgrades config");
            try {
                this.upgradeFileStatusService.update();
            } catch (HttpClientErrorException e) {
                if (!HttpStatus.NOT_MODIFIED.equals(e.getStatusCode())) {
                    LOGGER.warn("Exception thrown during upgrade", e);
                }
                return Optional.empty();
            }
            LOGGER.debug("Updated upgrade config");
            execution = this.upgradeService.start();
            while (execution.isRunning()) {
                //noinspection BusyWait
                Thread.sleep(500);
            }
            return Optional.of(execution);
        }
    }
}
