package io.openraven.services;

import org.apache.curator.framework.CuratorFramework;

import static java.lang.String.join;

public interface ZookeeperService {
    default String combine(String... paths) {
        return join("/", paths);
    }

    CuratorFramework getZookeeperClient();

    String combineRootAndContext();
}
