package io.openraven.services;

import io.openraven.UpgradeFileStatusConfig;
import io.openraven.models.UpgradeFileStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestOperations;

import java.net.URI;
import java.util.UUID;

@Component
public class UpgradeFileStatusServiceImpl implements UpgradeFileStatusService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpgradeFileStatusServiceImpl.class);
    private final RestOperations restOperations;
    private final UpgradeFileStatusConfig upgradeFileStatusConfig;

    public UpgradeFileStatusServiceImpl(RestOperations restOperations,
                                        UpgradeFileStatusConfig upgradeFileStatusConfig) {
        this.restOperations = restOperations;
        this.upgradeFileStatusConfig = upgradeFileStatusConfig;
    }

    @Override
    public UpgradeFileStatus update() {
        final String bucketName = upgradeFileStatusConfig.getName();
        final String bucketKey = upgradeFileStatusConfig.getKey();
        final String existingEtag = upgradeFileStatusConfig.getEtag();

        final URI uri = URI.create(String.format(
                "https://%s.s3.amazonaws.com/%s", bucketName, bucketKey));
        final RequestEntity<Void> request = RequestEntity.get(uri)
                .ifNoneMatch(existingEtag)
                .build();
        final ResponseEntity<String> response = restOperations.exchange(request, String.class);
        final HttpStatus statusCode = response.getStatusCode();

        if (statusCode.isError()) {
            LOGGER.warn("Fetch for {} did not pan out: {}", uri, response);
            throw new HttpClientErrorException(statusCode);
        } else if (HttpStatus.NOT_MODIFIED.equals(statusCode)) {
            // signal the not-modified
            throw new HttpClientErrorException(statusCode);
        }
        final String etag = response.getHeaders().getETag();
        String payload = response.getBody();
        upgradeFileStatusConfig.setEtag(etag);
        if (StringUtils.isEmpty(payload)) {
            // let the error checking on the runner handle it
            payload = "";
        }
        // this is unfortunately written into ZK but
        // **also** used as the contents of a temp file used to run helmfile
        upgradeFileStatusConfig.setPayload(payload);

        return upgradeFileStatusConfig.model();
    }

    @Override
    public UpgradeFileStatus model() {
        return this.upgradeFileStatusConfig.model();
    }

    @Override
    public UpgradeFileStatus reset() throws Exception {
        upgradeFileStatusConfig.setEtag(UUID.randomUUID().toString());
        upgradeFileStatusConfig.write();
        return this.upgradeFileStatusConfig.model();
    }

    @Override
    public void writeToZookeeper() throws Exception {
        upgradeFileStatusConfig.write();
    }
}
