package io.openraven;

import com.google.common.annotations.VisibleForTesting;
import io.openraven.models.UpgradeFileStatus;
import io.openraven.services.ZookeeperService;
import org.apache.curator.framework.api.transaction.CuratorMultiTransaction;
import org.apache.curator.framework.api.transaction.TransactionOp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;


@ConfigurationProperties(prefix = "openraven.upgrades")
public class UpgradeFileStatusConfig {

    private static final String OPENRAVEN_UPGRADES = "openraven/upgrades";
    private static final String ETAG = "etag";
    private static final String PAYLOAD = "payload";
    private final UpgradeFileStatus config = new UpgradeFileStatus();
    private final ZookeeperService zookeeperService;

    @Autowired
    public UpgradeFileStatusConfig(ZookeeperService zookeeperService) {
        this.zookeeperService = zookeeperService;
    }

    public long getFixedDelay() {
        return config.fixedDelay;
    }

    /**
     * @param fixedDelay unit is milliseconds. Delay of next scheduled task when current task finishes
     */
    public void setFixedDelay(long fixedDelay) {
        this.config.fixedDelay = fixedDelay;
    }


    public String getName() {
        return config.name;
    }

    /**
     * The name in the betas s3 bucket to look up beta participants
     *
     * @param name Key in the s3 bucket
     */
    public void setName(final String name) {
        this.config.name = name;
    }

    public String getKey() {
        return config.key;
    }

    /**
     * The key in the betas s3 bucket to look up beta participants
     *
     * @param key Key in the s3 bucket
     */
    public void setKey(final String key) {
        this.config.key = key;
    }

    public String getRegion() {
        return config.region;
    }

    /**
     * The region to look in for the s3 bucket
     *
     * @param region - the aws s3 region
     */
    public void setRegion(String region) {
        this.config.region = region;
    }

    public String getEtag() {
        return this.config.etag;
    }

    public void setEtag(String etag) {
        this.config.etag = etag;
    }

    public void write() throws Exception {
        CuratorMultiTransaction transaction = this.zookeeperService.getZookeeperClient().transaction();
        if (this.config.etag != null & this.config.payload != null) {
            TransactionOp transactionOp = this.zookeeperService.getZookeeperClient().transactionOp();
            String openravenUpgradesContext = createUpgradesContext();
            String etagLocation = openravenUpgradesContext + ETAG;
            String payloadLocation = openravenUpgradesContext + PAYLOAD;
            transaction.forOperations(
                    transactionOp.setData().forPath(etagLocation,
                            this.config.etag.getBytes()),
                    transactionOp.setData().forPath(payloadLocation, this.config.payload)
            );
        }
    }

    public UpgradeFileStatus model() {
        return config;
    }

    public void setPayload(String payload) {
        this.config.payload = payload.getBytes();
    }

    public byte[] getPayload() {
        return this.config.payload;
    }

    @VisibleForTesting
    protected String createUpgradesContext() {
        String rootAndContext = zookeeperService.combineRootAndContext();
        return String.join("/", rootAndContext, OPENRAVEN_UPGRADES, "");
    }
}
