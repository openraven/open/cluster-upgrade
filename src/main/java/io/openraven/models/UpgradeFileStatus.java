package io.openraven.models;

public class UpgradeFileStatus {
    public String name;
    public String key;
    public String region;
    public String etag;
    public long fixedDelay;
    public byte[] payload;
}
