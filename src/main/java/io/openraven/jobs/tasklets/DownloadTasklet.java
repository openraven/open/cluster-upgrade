package io.openraven.jobs.tasklets;

import io.openraven.UpgradeFileStatusConfig;
import io.openraven.analytics.DownloadTaskletAnalytics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;

@Component
public class DownloadTasklet implements Tasklet {
    private static final Logger LOGGER = LoggerFactory.getLogger(DownloadTasklet.class);

    private final UpgradeFileStatusConfig upgradeFileStatusConfig;
    private final DownloadTaskletAnalytics downloadTaskletAnalytics;

    public DownloadTasklet(UpgradeFileStatusConfig upgradeFileStatusConfig, DownloadTaskletAnalytics downloadTaskletAnalytics) {
        this.upgradeFileStatusConfig = upgradeFileStatusConfig;
        this.downloadTaskletAnalytics = downloadTaskletAnalytics;
    }

    @Override
    @Nullable
    public RepeatStatus execute(@NonNull StepContribution contribution, ChunkContext chunkContext) throws Exception {
        chunkContext.setAttribute("etag", "newVal");
        final byte[] payload = upgradeFileStatusConfig.getPayload();
        if (payload == null || payload.length == 0) {
            downloadTaskletAnalytics.downloadFailure(upgradeFileStatusConfig.getEtag());
            throw new IllegalStateException("Payload is null or empty. Set either in application.yml or zookeeper");
        }
        final File tempFile = File.createTempFile("deploy", "payload");
        try (FileOutputStream fos = new FileOutputStream(tempFile)) {
            fos.write(payload);
            fos.flush();
        }
        String tempFileAbsolutePath = tempFile.getAbsolutePath();
        LOGGER.debug("Helmfile location: {}", tempFileAbsolutePath);
        contribution.getStepExecution().getExecutionContext().put("filename", tempFileAbsolutePath);
        LOGGER.debug("DownloadTasklet chunkContext {}", chunkContext.toString());
        contribution.setExitStatus(ExitStatus.COMPLETED);
        downloadTaskletAnalytics.downloadSuccessful(upgradeFileStatusConfig.getEtag());
        return RepeatStatus.FINISHED;
    }
}
