package io.openraven.jobs.tasklets;

import io.openraven.UpgradeFileStatusConfig;
import io.openraven.analytics.RunTaskletAnalytics;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.LogOutputStream;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.exec.environment.DefaultProcessingEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Component
public class RunTasklet implements Tasklet {

    private static final Logger LOGGER = LoggerFactory.getLogger(RunTasklet.class);
    private final RunTaskletAnalytics runTaskletAnalytics;
    private final UpgradeFileStatusConfig upgradeFileStatusConfig;

    public RunTasklet(RunTaskletAnalytics runTaskletAnalytics, UpgradeFileStatusConfig upgradeFileStatusConfig) {
        this.upgradeFileStatusConfig = upgradeFileStatusConfig;
        this.runTaskletAnalytics = runTaskletAnalytics;
    }

    @Override
    public RepeatStatus execute(@NonNull StepContribution contribution, @NonNull ChunkContext chunkContext) throws Exception {
        List<String> outputLineList = new ArrayList<>();
        String filename = (String) contribution
                .getStepExecution()
                .getJobExecution()
                .getExecutionContext()
                .get("filename");
        String concurrency = System.getenv().getOrDefault("HELMFILES_CONCURRENCY", "1");
        CommandLine commandLine = new CommandLine("helmfile");
        commandLine.addArguments(String.format("--allow-no-matching-release -f %s apply", filename));
        commandLine.addArguments(String.format("--concurrency %s", concurrency));
        LOGGER.debug("Executable: {}", commandLine.getExecutable());
        LOGGER.debug("Filename: {}", filename);
        Map<String, String> environment = createEnvironment();
        int returnCode = 0;
        try {
            DefaultExecutor defaultExecutor = new DefaultExecutor();
            defaultExecutor.setStreamHandler(new PumpStreamHandler(new LogOutputStream() {
                @Override
                protected void processLine(String line, int logLevel) {
                    outputLineList.add(line);
                }
            }));
            returnCode = defaultExecutor.execute(commandLine, environment);
        } catch (ExecuteException executeException) {
            returnCode = executeException.getExitValue();
            LOGGER.debug("Execution Exception thrown, return code is {}", returnCode, executeException);
        } catch (IOException e) {
            LOGGER.error("Error during execute", e);
        }
        String output = String.join("\n", outputLineList);
        if (returnCode == 0) {
            contribution.setExitStatus(ExitStatus.COMPLETED);
            runTaskletAnalytics.runSuccessful(output);
        } else {
            contribution.setExitStatus(ExitStatus.FAILED.addExitDescription("Failed with code " + returnCode));
            runTaskletAnalytics.runFailure(output);
        }
        chunkContext.setAttribute("exitCode", returnCode);
        chunkContext.setAttribute("output", output);
        chunkContext.setAttribute("runTasklet", "runTasklet");
        LOGGER.debug("Output {}", output);
        LOGGER.debug("RunTasklet chunkContext {}", chunkContext);
        return RepeatStatus.FINISHED;
    }

    private Map<String, String> createEnvironment() throws IOException {
        DefaultProcessingEnvironment defaultProcessingEnvironment = new DefaultProcessingEnvironment();
        Map<String, String> environment = defaultProcessingEnvironment.getProcEnvironment();
        String name = this.upgradeFileStatusConfig.getName();
        if (!isBlank(name)) {
            environment.put("HELM_S3_BUCKET", name);
        }
        String region = this.upgradeFileStatusConfig.getRegion();
        if (!isBlank(region)) {
            environment.put("HELM_S3_REGION", region);
        }
        String key = this.upgradeFileStatusConfig.getKey();
        if (!isBlank(key)) {
            environment.put("HELM_S3_KEY", key);
        }
        return environment;
    }
}
