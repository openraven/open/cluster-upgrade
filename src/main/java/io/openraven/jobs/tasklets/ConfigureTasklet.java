package io.openraven.jobs.tasklets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

/**
 * Do the configuration necessary
 */
@Component
public class ConfigureTasklet implements Tasklet {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigureTasklet.class);

    @Override
    public RepeatStatus execute(@NonNull StepContribution contribution, ChunkContext chunkContext) {
        chunkContext.setAttribute("etag", "oldEtag");
        LOGGER.debug("ConfigureTasklet chunkContext {}", chunkContext.toString());
        return RepeatStatus.FINISHED;
    }
}
