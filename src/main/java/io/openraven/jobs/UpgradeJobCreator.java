package io.openraven.jobs;

import io.openraven.jobs.tasklets.ConfigureTasklet;
import io.openraven.jobs.tasklets.DownloadTasklet;
import io.openraven.jobs.tasklets.RunTasklet;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.listener.ExecutionContextPromotionListener;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.stereotype.Component;


@Component
public class UpgradeJobCreator {

    public static final String UPGRADE_JOB_NAME = "upgrade";
    private static final String CONFIGURE_STEP_NAME = "configure";
    private static final String DOWNLOAD_STEP_NAME = "download";
    private static final String RUN_STEP_NAME = "run";

    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final ConfigureTasklet configureTasklet;
    private final DownloadTasklet downloadTasklet;
    private final RunTasklet runTasklet;

    public UpgradeJobCreator(JobBuilderFactory jobBuilderFactory,
                             StepBuilderFactory stepBuilderFactory,
                             ConfigureTasklet configureTasklet,
                             DownloadTasklet downloadTasklet,
                             RunTasklet runTasklet) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.configureTasklet = configureTasklet;
        this.downloadTasklet = downloadTasklet;
        this.runTasklet = runTasklet;
    }

    public Job create() {
        return this.jobBuilderFactory
                .get(UPGRADE_JOB_NAME)
                .start(configureStep(configureTasklet))
                .next(downloadStep(downloadTasklet))
                .next(runStep(runTasklet))
                .build();
    }

    private Step configureStep(ConfigureTasklet configureTasklet) {
        return createStep(CONFIGURE_STEP_NAME, configureTasklet);
    }

    private Step downloadStep(DownloadTasklet downloadTasklet) {
        return createStep(DOWNLOAD_STEP_NAME, downloadTasklet);
    }

    private Step runStep(RunTasklet runTasklet) {
        return createStep(RUN_STEP_NAME, runTasklet);
    }

    private Step createStep(String name, Tasklet tasklet) {
        return this.stepBuilderFactory.get(name)
                .tasklet(tasklet)
                .allowStartIfComplete(true)
                .listener(promotionListener())
                .build();
    }
    private ExecutionContextPromotionListener promotionListener() {
        ExecutionContextPromotionListener listener = new ExecutionContextPromotionListener();

        listener.setKeys(new String[] {"filename" });

        return listener;
    }

}
