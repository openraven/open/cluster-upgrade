package io.openraven;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "openraven.app.v1.cloud-ingestion")
public class CloudIngestionProperties {

    private String integration;
    private Analytics analytics;

    public static class Analytics {
        boolean enabled;
        String clusterId;

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }


        public String getClusterId() {
            return clusterId;
        }

        public void setClusterId(String clusterId) {
            this.clusterId = clusterId;
        }
    }

    public String getIntegration() {
        return integration;
    }

    /**
     *
     * @param integration The event name in segment. Should identify an activity for telemetry.
     */
    public void setIntegration(String integration) {
        this.integration = integration;
    }

    public Analytics getAnalytics() {
        return analytics;
    }

    public void setAnalytics(Analytics analytics) {
        this.analytics = analytics;
    }
}
