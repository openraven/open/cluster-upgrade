package io.openraven.analytics;

import org.springframework.stereotype.Component;

@Component
public class DownloadTaskletAnalytics {
    private static final String DOWNLOAD_SUCCESSFUL = "DownloadSuccessful";
    private static final String DOWNLOAD_FAILED = "DownloadFailed";
    private static final String NO_PAYLOAD_SET_FORMAT = "NO PAYLOAD SET for etag %s";
    private final OpenravenAnalytics openravenAnalytics;

    public DownloadTaskletAnalytics(OpenravenAnalytics openravenAnalytics) {
        this.openravenAnalytics = openravenAnalytics;
    }

    public void downloadFailure(String etag) {
        openravenAnalytics.sendAnalytics(String.format(NO_PAYLOAD_SET_FORMAT, etag), DOWNLOAD_FAILED);
    }

    public void downloadSuccessful(String etag) {
        openravenAnalytics.sendAnalytics(etag, DOWNLOAD_SUCCESSFUL);
    }
}
