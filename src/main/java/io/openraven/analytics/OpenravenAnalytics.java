package io.openraven.analytics;

import java.util.Optional;

import com.google.common.collect.ImmutableMap;
import com.segment.analytics.Analytics;
import com.segment.analytics.messages.TrackMessage;
import io.openraven.CloudIngestionProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OpenravenAnalytics {
    private final CloudIngestionProperties cloudIngestionProperties;
    private static final Logger LOGGER = LoggerFactory.getLogger(OpenravenAnalytics.class);
    private final CloudIngestionProperties.Analytics cloudIngestionPropertiesAnalytics;
    private final Analytics analytics;
    private final String clusterName;
    private final String deployChannel;

    public OpenravenAnalytics(CloudIngestionProperties cloudIngestionProperties,
                              @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
                              @Autowired(required = false) Analytics analytics) {
        this.cloudIngestionProperties = cloudIngestionProperties;
        this.cloudIngestionPropertiesAnalytics = cloudIngestionProperties.getAnalytics();
        this.analytics = analytics;
        clusterName = Optional.ofNullable(System.getenv("CLUSTER_NAME")).orElse("");
        deployChannel = Optional.ofNullable(System.getenv("OPENRAVEN_UPGRADES_NAME")).orElse("");
    }

    public void sendAnalytics(String payload, String key) {
        if (!cloudIngestionPropertiesAnalytics.isEnabled() || analytics == null) {
            return;
        }
        try {
            // DEPRECATED: this can be removed after everything has transitioned
            // over to the new "key" based events
            analytics.enqueue(TrackMessage.builder(cloudIngestionProperties.getIntegration())
                    .userId(cloudIngestionPropertiesAnalytics.getClusterId())
                    .properties(ImmutableMap.<String, String>builder()
                            .put(key, payload)
                            .build()
                    )
            );
            analytics.enqueue(TrackMessage.builder(key)
                    .userId(cloudIngestionPropertiesAnalytics.getClusterId())
                    .properties(ImmutableMap.<String, String>builder()
                            .put("clusterName", clusterName)
                            .put("deployChannel", deployChannel)
                            .put("integration", cloudIngestionProperties.getIntegration())
                            .put("payload", payload)
                            .build()
                    )
            );
        } catch (Exception e) {
            LOGGER.debug("Telemetry send failed with exception", e);
        }
    }
}
