package io.openraven.analytics;

import org.springframework.stereotype.Component;

@Component
public class RunTaskletAnalytics {
    private static final String INSTALLATION_FAILED = "InstallationFailed";
    private static final String INSTALLATION_SUCCESSFUL = "InstallationSuccessful";
    private final OpenravenAnalytics openravenAnalytics;

    public RunTaskletAnalytics(OpenravenAnalytics openravenAnalytics) {
        this.openravenAnalytics = openravenAnalytics;
    }


    public void runSuccessful(String output) {
        openravenAnalytics.sendAnalytics(output, INSTALLATION_SUCCESSFUL);
    }
    public void runFailure(String output) {
        openravenAnalytics.sendAnalytics(output, INSTALLATION_FAILED);
    }
}
