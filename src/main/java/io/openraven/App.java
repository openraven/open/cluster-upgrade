package io.openraven;

import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.Banner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.zookeeper.ZookeeperProperties;
import org.springframework.cloud.zookeeper.config.ZookeeperConfigProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class App {
    public static void main(String[] args) {
        new SpringApplicationBuilder(App.class)
                .bannerMode(Banner.Mode.OFF)
                .web(WebApplicationType.SERVLET)
                .run(args);
    }

    @EnableScheduling
    @Configuration
    @EnableConfigurationProperties({
            UpgradeFileStatusConfig.class,
            UpgradeConfig.class,
            CloudIngestionProperties.class,
            ZookeeperProperties.class,
            ZookeeperConfigProperties.class
    })
    public static class Config {
        /** In lieu of dragging Jackson et al in via {@code HttpMessageConvertersAutoConfiguration}. */
        @Bean
        public RestTemplate getRestTemplate() {
            return new RestTemplateBuilder().build();
        }
    }

    @EnableBatchProcessing
    @EnableAutoConfiguration(exclude = {
            DataSourceAutoConfiguration.class,
            DataSourceTransactionManagerAutoConfiguration.class,
    })
    public static class BatchConfiguration extends DefaultBatchConfigurer {

    }

    @Configuration
    static class OpenravenWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .csrf().disable()
                    .authorizeRequests().antMatchers("/actuator/**").permitAll()
                    .and().authorizeRequests().antMatchers("/**").authenticated()
                    .antMatchers("/**").hasIpAddress("127.0.0.1").anyRequest().permitAll();
        }
    }
}
