package io.openraven.configuration;

import org.apache.curator.RetryPolicy;
import org.apache.curator.drivers.TracerDriver;
import org.apache.curator.ensemble.EnsembleProvider;
import org.apache.curator.framework.CuratorFramework;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.cloud.zookeeper.CuratorFactory;
import org.springframework.cloud.zookeeper.CuratorFrameworkCustomizer;
import org.springframework.cloud.zookeeper.ZookeeperProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class ZookeeperConfig {
    @Bean(destroyMethod = "close")
    public CuratorFramework curatorFramework(ZookeeperProperties properties,
                                             RetryPolicy retryPolicy,
                                             ObjectProvider<CuratorFrameworkCustomizer> optionalCuratorFrameworkCustomizerProvider,
                                             ObjectProvider<EnsembleProvider> optionalEnsembleProvider,
                                             ObjectProvider<TracerDriver> optionalTracerDriverProvider) throws Exception {
        return CuratorFactory.curatorFramework(properties, retryPolicy,
                optionalCuratorFrameworkCustomizerProvider::orderedStream, optionalEnsembleProvider::getIfAvailable,
                optionalTracerDriverProvider::getIfAvailable);
    }

    @Bean
    @Profile("!test")
    public RetryPolicy exponentialBackoffRetry(ZookeeperProperties properties) {
        return CuratorFactory.retryPolicy(properties);
    }
}
