package io.openraven;

import io.openraven.services.ZookeeperService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class UpgradeFileStatusConfigTest {

    private UpgradeFileStatusConfig config;

    @Mock
    private ZookeeperService zookeeperService;

    @BeforeEach
    void setUp() {
        config = new UpgradeFileStatusConfig(zookeeperService);
    }

    @Test
    void testZkPath() {
        Mockito.doReturn("/config/application").when(zookeeperService).combineRootAndContext();

        String upgradesContext = config.createUpgradesContext();

        assertEquals("/config/application/openraven/upgrades/", upgradesContext);
    }
}