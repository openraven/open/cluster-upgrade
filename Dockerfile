FROM registry.gitlab.com/openraven/open/orvn-java:11

# hello, if you change these, please change them in build-image, also
# https://gitlab.com/openraven/open/build-image
ARG helmfile_version="0.150.0"
ARG helm_diff_version="3.6.0"
ARG helm_version="v3.11.1"
# courtesy of https://docs.aws.amazon.com/eks/latest/userguide/install-kubectl.html
ARG kubectl_version="1.22.17"
ARG kubectl_dt='2023-01-11'
# this is set by buildx
ARG TARGETARCH

RUN set -e ;\
    export DEBIAN_FRONTEND=noninteractive ;\
    apt-get update ;\
    apt-get install -y ca-certificates git curl jq ;\
    rm -rf /var/lib/apt/lists/* /var/cache/apt
RUN set -e ;\
    kubectl_url="https://s3.us-west-2.amazonaws.com/amazon-eks/${kubectl_version}/${kubectl_dt}/bin/linux/${TARGETARCH}/kubectl" ;\
    curl -fsSLo /bin/kubectl "$kubectl_url" ;\
    chmod a+x   /bin/kubectl
RUN set -e ;\
    helm_tgz="https://get.helm.sh/helm-${helm_version}-linux-${TARGETARCH}.tar.gz" ;\
    curl -fsSL "$helm_tgz" | tar -xzvf - ;\
    mv linux-${TARGETARCH}/helm /bin/helm ;\
    rm -rf linux-${TARGETARCH} ;\
    chmod +x /bin/helm
RUN /bin/helm plugin install --version $helm_diff_version https://github.com/databus23/helm-diff
RUN set -e ;\
    helmfile_url="https://github.com/helmfile/helmfile/releases/download/v${helmfile_version}/helmfile_${helmfile_version}_linux_${TARGETARCH}.tar.gz" ;\
    curl -fsSL "$helmfile_url" | tar -xvzf - -C /bin helmfile

# Add the service itself
ARG JAR_FILE
ARG JAR_DIR
ADD ${JAR_DIR}/${JAR_FILE} /root/service.jar
RUN if [ ! -f /root/service.jar ]; then \
        echo "Expected /root/service.jar to be a file" >&2 ;\
        ls -la /root/service.jar >&2 ;\
        exit 1 ;\
    fi
